//
//  DeviceInterface.swift
//  ChallengeDevice
//
//  Created by Eric Betts on 7/6/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol DeviceInterfaceDelegate {
    func incomingReport(report: NSData)
}

class DeviceInterface : NSObject, CBPeripheralManagerDelegate {
    static let DeviceName = "Cbxrzba TB Cyhf".rot13
    
    static let ClientCharacteristicConfigurationUUID = CBUUID(string: "00002902-0000-1000-8000-00805f9b34fb")
    //tvOS 9.0: CBUUIDClientCharacteristicConfigurationString
    static let ClientCharacteristicConfiguration = CBMutableCharacteristic(type: ClientCharacteristicConfigurationUUID, properties: [.Read, .Write, .WriteWithoutResponse], value: nil, permissions: [.Readable, .Writeable])
    
    
    var deviceService : CBMutableService {
        let device_service_uuid = CBUUID(string: "21c50462-67cb-63a3-5c4c-82b5b9939aeb")
        let led_uuid = CBUUID(string: "21c50462-67cb-63a3-5c4c-82b5b9939aec")
        let button_uuid = CBUUID(string: "21c50462-67cb-63a3-5c4c-82b5b9939aed")
        
        let control_service = CBMutableService(type: device_service_uuid, primary: true)
        let outputChar = CBMutableCharacteristic(type: led_uuid, properties: [.WriteWithoutResponse, .Write], value: nil, permissions: .Writeable)
        let inputChar = CBMutableCharacteristic(type: button_uuid, properties: .Notify, value: nil, permissions: .Readable)
        
        control_service.characteristics = [inputChar, outputChar]
        return control_service
    }
    
    
    var batteryService : CBMutableService {
        //https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.battery_service.xml
        let battery_service_uuid = CBUUID(string: "0000180F-0000-1000-8000-00805f9b34fb")
        //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.battery_level.xml
        let battery_uuid = CBUUID(string: "00002A19-0000-1000-8000-00805f9b34fb")
        
        let battery_service : CBMutableService = CBMutableService(type: battery_service_uuid, primary: true)
        let battery_level = CBMutableCharacteristic(type: battery_uuid, properties: .Read, value: nil, permissions: .Readable)
        
        battery_service.characteristics = [battery_level]
        return battery_service
    }
    
    var firmwareService : CBMutableService {
        let firmware_service_uuid = CBUUID(string: "0000fef5-0000-1000-8000-00805f9b34fb")
        let update_uuid = CBUUID(string: "21c50462-67cb-63a3-5c4c-82b5b9939aef")
        let version_uuid = CBUUID(string: "21c50462-67cb-63a3-5c4c-82b5b9939af0")
        
        let firmware_service : CBMutableService = CBMutableService(type: firmware_service_uuid, primary: true)
        let update = CBMutableCharacteristic(type: update_uuid, properties: .Notify, value: nil, permissions: .Readable)
        let version = CBMutableCharacteristic(type: version_uuid, properties: .Notify, value: nil, permissions: .Readable)
        
        firmware_service.characteristics = [version, update]
        return firmware_service
    }
    
    var certService : CBMutableService {
        let cert_service_uuid = CBUUID(string: "bbe87709-5b89-4433-ab7f-8b8eef0d8e37")
        let s2c_uuid = CBUUID(string: "bbe87709-5b89-4433-ab7f-8b8eef0d8e3a")
        let c2s_uuid = CBUUID(string: "bbe87709-5b89-4433-ab7f-8b8eef0d8e38")
        let commands_uuid = CBUUID(string: "bbe87709-5b89-4433-ab7f-8b8eef0d8e39")
        
        let cert_service : CBMutableService = CBMutableService(type: cert_service_uuid, primary: true)
        let s2c = CBMutableCharacteristic(type: s2c_uuid, properties: .Notify, value: nil, permissions: .Readable)
        let c2s = CBMutableCharacteristic(type: c2s_uuid, properties: [.WriteWithoutResponse, .Write], value: nil, permissions: .Writeable)
        let commands = CBMutableCharacteristic(type: commands_uuid, properties: .Notify, value: nil, permissions: .Readable)
        
        cert_service.characteristics = [s2c, c2s, commands]
        return cert_service
    }
    
    //Battery service pretty much handled by native device
    var services : [CBMutableService] {
        return [deviceService, certService, firmwareService/*, batteryService*/]
    }
    
    var peripheralManager : CBPeripheralManager?
    var readCharacteristic : CBMutableCharacteristic?
    
    var delegate : DeviceInterfaceDelegate? = nil
    
    func start(delegate: DeviceInterfaceDelegate) {
        self.delegate = delegate
        let queue = dispatch_get_main_queue()
        peripheralManager = CBPeripheralManager(delegate: self, queue: queue)
    }
    
    func stop() {
        if let peripheral = self.peripheralManager {
            peripheral.removeAllServices()
            peripheral.stopAdvertising()
        }
    }
    
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        peripheral.removeAllServices()
        switch peripheral.state {
        case .PoweredOff:
            print("BLE OFF")
        case .PoweredOn:
            print("BLE ON")
            services.forEach({ peripheral.addService($0) })
            let advertisementData : [String: AnyObject] = [
                CBAdvertisementDataLocalNameKey: DeviceInterface.DeviceName,
                CBAdvertisementDataSolicitedServiceUUIDsKey: services.map({ $0.UUID })
            ]
            peripheral.startAdvertising(advertisementData)
            print("Adverting: \(advertisementData)")
        case .Unknown:
            print("NOT RECOGNIZED")
        case .Unsupported:
            print("BLE NOT SUPPORTED")
        case .Resetting:
            print("BLE NOT SUPPORTED")
        default:
            print("Error")
        }
    }
    
    func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
        print("peripheralManagerDidStartAdvertising")
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didSubscribeToCharacteristic characteristic: CBCharacteristic) {
        print("didSubscribeToCharacteristic \(characteristic.UUID)")
        self.readCharacteristic = characteristic.mutableCopy() as? CBMutableCharacteristic
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic) {
        print("didUnsubscribeFromCharacteristic \(characteristic.UUID)")
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, didReceiveWriteRequests requests: [CBATTRequest]) {
        for request in requests {
            peripheral.respondToRequest(request, withResult: CBATTError.Success)
            if let newValue = request.value {
                print("<= \(newValue)")
                self.delegate?.incomingReport(newValue)
            }
        }
    }
    
    func outgoingReport(report: NSData) {
        guard let peripheralManager = self.peripheralManager else {
            print("Attempted to send report when peripheralManager was not defined")
            return
        }
        
        guard peripheralManager.state == CBPeripheralManagerState.PoweredOn else {
            print("Attempted to send report when peripheralManager was not powered on")
            return
        }
        
        if let readCharacteristic = self.readCharacteristic {
            print("=> \(report)")
            peripheralManager.updateValue(report, forCharacteristic: readCharacteristic, onSubscribedCentrals: nil)
        }
    }
    
}