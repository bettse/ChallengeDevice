//
//  Device.swift
//  ChallengeDevice
//
//  Created by Eric Betts on 7/6/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation

class Device : DeviceInterfaceDelegate {
    static let singleton = Device()
    
    let interface = DeviceInterface()
    
    func start() {
        interface.start(self)
    }
    
    func incomingReport(report: NSData) {
        print("Device incomingReport: \(report)")
    }
}