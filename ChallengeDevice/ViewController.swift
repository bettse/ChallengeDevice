//
//  ViewController.swift
//  ChallengeDevice
//
//  Created by Eric Betts on 7/6/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let device = Device.singleton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        device.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}